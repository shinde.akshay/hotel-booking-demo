package com.demo.hotelbooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.hotelbooking.model.HotelBooking;
import com.demo.hotelbooking.service.HotelBookingService;

@RestController
@RequestMapping("/hotelBooking")
public class HotelBookingController {

	@Autowired
	HotelBookingService hotelBookingService;

	@GetMapping("/getAll")
	public List<HotelBooking> getAllBookingsAPI() {
		return hotelBookingService.getAllBookings();
	}

	@GetMapping(path = "/getById/{id}")
	public HotelBooking getOneById(@PathVariable(name = "id") Long id) {
		return hotelBookingService.getOneById(id);
	}

	@PostMapping(path = "/createBooking", consumes = "application/json", produces = "application/json")
	public ResponseEntity<HotelBooking> createBookingAPI(@RequestBody HotelBooking hotelBooking) {
		HotelBooking createdOrder = hotelBookingService.createBooking(hotelBooking);

		return new ResponseEntity<HotelBooking>(createdOrder, HttpStatus.CREATED);
	}
}