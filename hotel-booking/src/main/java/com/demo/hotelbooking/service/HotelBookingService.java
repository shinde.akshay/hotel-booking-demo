package com.demo.hotelbooking.service;

import java.util.List;

import com.demo.hotelbooking.model.HotelBooking;

public interface HotelBookingService {
	List<HotelBooking> getAllBookings();

	HotelBooking getOneById(Long hotelId);

	HotelBooking createBooking(HotelBooking hotelBooking);
}
