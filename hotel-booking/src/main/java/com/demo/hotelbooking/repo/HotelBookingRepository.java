package com.demo.hotelbooking.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.hotelbooking.model.HotelBooking;

@Repository
public interface HotelBookingRepository extends CrudRepository<HotelBooking, Long> {

}
