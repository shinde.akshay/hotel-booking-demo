--Create Table Script

CREATE TABLE `hotel_booking`.`booking_details` (
  `booking_id` INT(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` INT(11) DEFAULT NULL,
  `hotel_name` VARCHAR(45) DEFAULT NULL,
  `checkin_date` DATETIME DEFAULT NULL,
  `checkout_date` DATETIME DEFAULT NULL,
  `cust_name` VARCHAR(50) DEFAULT NULL,
  `cust_email` VARCHAR(255) DEFAULT NULL,
  `cust_contact` VARCHAR(15) DEFAULT NULL,
  `room_id` INT(11) DEFAULT NULL,
  `room_name` VARCHAR(45) DEFAULT NULL,
  `guest_count` INT(11) DEFAULT NULL,
  `total_amount` INT(11) DEFAULT NULL,
  PRIMARY KEY (`booking_id`));